<?php

use App\Http\Controllers\FileController;
use App\Http\Controllers\FolderController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/tree-view', [HomeController::class, 'tree_view'])->name('tree-view');

Route::post('/folders', [FolderController::class, 'store'])->name('folders.store');
Route::post('/folders/rename', [FolderController::class, 'rename_folder'])->name('folders.rename');

Route::post('/files', [FileController::class, 'store'])->name('files.store');
Route::post('/files/rename', [FileController::class, 'rename_file'])->name('files.rename');

Route::get('/{folder_id}', [FolderController::class, 'open_folder']);
