@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-12 text-right">
                <form action="{{ route('files.store') }}" method="post" id="fileForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="folder_id" value="{{ $parent_id }}">
                    <input type="file" name="file" id="uploadFile" class="d-none" onchange="submitForm()">
                </form>
                <label for="uploadFile" class=" float-end">
                    <a type="button" class="btn btn-outline-primary float-end">Upload File</a>
                </label>
                <button type="button" class="btn btn-outline-success float-end me-2" data-bs-toggle="modal"
                    data-bs-target="#addFolderModal">Add Folder</button>
            </div>
        </div>

        <div class="row mb-4">
            <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    @if (!$path_chain)
                        <li class="breadcrumb-item active">Home</li>
                    @else
                        <li class="breadcrumb-item active"><a href="/">Home</a></li>
                        @foreach ($path_chain as $folder)
                            @if ($folder == end($path_chain))
                                <li class="breadcrumb-item active">{{ $folder->name }}</li>
                            @else
                                <li class="breadcrumb-item "><a href="/{{ $folder->id }}">{{ $folder->name }}</a></li>
                            @endif
                        @endforeach
                        {{-- <li class="breadcrumb-item active" aria-current="page">Library</li> --}}
                    @endif
                </ol>
            </nav>
        </div>

        @if (!count($folders) && !count($files))
            <div class="row mt-5">
                <h4 class="text-center text-black-50 fst-italic">This folder is empty !</h4>
            </div>
        @endif
        <div class="row">
            @foreach ($folders as $folder)
                <div class="col-md-3 col-lg-2 col-sm-4 co-xs-6 mb-4 ">
                    <div class="card text-center">
                        <img src="{{ asset('assets/images/folder.png') }}" data-bs-toggle="tooltip"
                            data-bs-placement="top" title="Double click to open folder" class="card-img-top pointer"
                            alt="..." ondblclick="openFolder({{ $folder->id }})">
                        <div class="card-body">
                            <h5 class="card-title " data-bs-toggle="tooltip" data-bs-placement="top"
                                title="Double click to rename"
                                ondblclick="renameFolder({{ $folder->id }}, '{{ $folder->name }}' )">
                                {{ $folder->name }}
                            </h5>
                        </div>
                    </div>
                </div>
            @endforeach

            @foreach ($files as $file)
                <div class="col-md-3 col-lg-2 col-sm-4 co-xs-6 mb-4 ">
                    <div class="card text-center">
                        <img src="{{ asset('assets/images/file.png') }}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title" title="Double click to rename"
                                ondblclick="renameFile({{ $file->id }}, '{{ $file->name }}' )">
                                {{ $file->name }}
                            </h5>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="row"></div>
    </div>
@endsection


@section('modals')
    {{-- Add New Folder Modal --}}
    <div class="modal fade" id="addFolderModal" tabindex="-1" aria-labelledby="addFolderModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {{-- <div class="modal-header">
                    <h5 class="modal-title" id="addFolderModalLabel">Add New Folder</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div> --}}
                <form action="{{ route('folders.store') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="mb-3 row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="hidden" name="parent_id" value="{{ $parent_id }}">
                                <input type="text" class="form-control" name="name" id="name">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Rename Folder Modal --}}
    <div class="modal fade" id="renameFolderModal" tabindex="-1" aria-labelledby="renameFolderModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('folders.rename') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-sm-8">
                                <input type="hidden" name="folder_id" value="" id="renameFolderId">
                                <input type="text" class="form-control" name="name" id="renameFolderInput">
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">Rename Folder</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Rename File Modal --}}
    <div class="modal fade" id="renameFileModal" tabindex="-1" aria-labelledby="renameFileModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('files.rename') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="mb-3 row">
                            <div class="col-sm-8">
                                <input type="hidden" name="file_id" value="" id="renameFileId">
                                <input type="text" class="form-control" name="name" id="renameFileInput">
                            </div>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">Rename File</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var renameFolderModal = new bootstrap.Modal(document.getElementById('renameFolderModal'), {})
        var renameFileModal = new bootstrap.Modal(document.getElementById('renameFileModal'), {})

        function openFolder(folderId) {
            window.location.replace("/" + folderId);
        }

        function renameFolder(folderId, currentName) {
            renameFolderModal.show();
            $('#renameFolderId').val(folderId)
            $('#renameFolderInput').val(currentName)
        }

        function renameFile(fileId, currentName) {
            renameFileModal.show();
            $('#renameFileId').val(fileId)
            $('#renameFileInput').val(currentName)
        }

        function submitForm() {
            $("#fileForm").submit();
        }
    </script>
@endsection
