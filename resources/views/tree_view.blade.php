@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-12">
                <ul class="">
                    @foreach ($folders as $folder)

                        

                        <li class="text-primary">
                            <strong>{{ $folder->name }}</strong>
                            @if (count($folder->children))
                                @include('partials.manageChild', [
                                    'children' => $folder->children,
                                ])
                            @endif
                        </li>

                        @endforeach
                        @foreach ($root_files as $file)
                            <li class="text-secondary">
                                {{ $file->name }}
                            </li>
                        @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection
