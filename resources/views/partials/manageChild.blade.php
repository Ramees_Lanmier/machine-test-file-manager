<ul>

    @foreach ($children as $child)
        
        <li class="text-primary">
            <strong>{{ $child->name }}</strong>
            @if (count($child->children))
                @include('partials.manageChild', ['children' => $child->children])
            @endif
        </li>
        @foreach ($child->files as $file)
            <li class="text-secondary">
                {{ $file->name }} 
            </li>
        @endforeach
    @endforeach
</ul>
