<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Folder;
use Illuminate\Http\Request;

class FolderController extends Controller
{
    public function store(Request $request)
    {
        $folder = new Folder();
        $folder->name = $request->name;
        $folder->parent_id = $request->parent_id;
        $folder->save();

        return redirect()->back();
    }


    public function open_folder($folder_id)
    {
        $currentFolder = Folder::find($folder_id);

        // $i =0;
        // $breadcrumb_data[$i]['name'] = $currentFolder->name;
        // $breadcrumb_data[$i]['id'] = $currentFolder->id;

        // $temp_folder = $currentFolder;
        // $i++;

        // while ($temp_folder->parentFolder) {
        //     dump($temp_folder->parentFolder->toarray());
        //     $breadcrumb_data[$i]['name'] = $temp_folder->name;
        //     $breadcrumb_data[$i]['id'] = $temp_folder->id;
        //     $i++;
        //     $temp_folder = $temp_folder->parentFolder;
        // }
        

        $temp_folder = $currentFolder;

        $reverse_path_chain[] = $temp_folder;
        while ($temp_folder->parentFolder) {
            $reverse_path_chain[] = $temp_folder->parentFolder;
            $temp_folder = $temp_folder->parentFolder;
        }

        $path_chain = array_reverse($reverse_path_chain);
        $folders = Folder::where('parent_id', $folder_id)->get();

        $files = File::where('folder_id', $folder_id)->get();
        return view('index', [
            'parent_id' => $folder_id,
            'folders' => $folders,
            'files' => $files,
            'path_chain' => $path_chain
        ]);
    }

    public function rename_folder(Request $request)
    {
        $folder = Folder::find($request->folder_id);
        $folder->name = $request->name;
        $folder->save();

        return redirect()->back();
    }
}
