<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Folder;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        $folders = Folder::where('parent_id', NULL)->get();
        $files = File::where('folder_id', NULL)->get();
        $parent_id = '';
        return view('index', [
            'parent_id' => $parent_id,
            'folders' => $folders,
            'files' => $files,
            'path_chain' => NULL
        ]);
    }

    public function tree_view()
    {
        $folders = Folder::where('parent_id', NULL)->get();
        $root_files = File::where('folder_id', NULL)->get();
        $all_folders = Folder::all();
        // dd($folders[2]->children->toArray());
        return view('tree_view', [
            'folders' => $folders,
            'all_folders' => $all_folders,
            'root_files' => $root_files
        ]);

    }
}
