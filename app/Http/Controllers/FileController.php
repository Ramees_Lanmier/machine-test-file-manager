<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function store(Request $request)
    {
        // dd($request->file('file')->getClientOriginalName());
        $file_name = $request->file('file')->getClientOriginalName();

        $file = new File();
        $file->path = Storage::disk('public')->put('', $request->file);
        $file->name =$file_name;
        $file->folder_id =$request->folder_id;
        $file->save();

        return redirect()->back();
    }

    public function rename_file(Request $request)
    {
        $file = File::find($request->file_id);
        $file -> name = $request->name;
        $file->save();

        return redirect()->back();
    }
}
